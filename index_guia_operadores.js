// Operadores

// Operadores Aritmeticos
//  Suma +
//  Resta -
//  Multiplicar *
//  Dividir /
//  Modulo % Obtiene el resto de una division.

console.log("Operador de Suma: ", 1 + 1)

console.log("Multiplicacion:", 3 * 3)

console.log("Resta:", 5 - 4)

console.log("Division", 12 / 4)

console.log("Modulo", 4 % 12) 

// Operadores de comparacion
//  Mayor que >
// Menor que <
// Mayor o igual que >=
// Menor o igual que <=
//  valorA == "hola" Evaluacion parcial del dato (Solo tipo dato), 
//  valorA === 12 Evaluacion estricta del dato (Tipo del dato y El valor)

// Operadores de asignación & adicion
// =
// +=

let valorA = 10;
let valorB = "Hola como estas";
let valorC = 6;
const constA = "www.google.com/search=" + valorC;

// const valorC = 12;
// const valorD = 8;

// Cuando una operacion aritmetica no sea logica, por ejemplo restar un texto con un numero. Devolverá NaN 
//  NaN = Not A Number (No es un numero)
console.log("Operador de Suma: ", valorA - valorB)

// Resta con variables de tipo Number (Numero)
console.log("Operador de Suma: ", valorA - valorC)


// Operador asignancion => 
valorA = 5;
console.log("Valor de valorA nuevo :", valorA)

// Operadores logicos
//  if ()
//  else if ()
//  else 

// let valorA = 10;
// let valorB = "Hola como estas";
// let valorC = 6;

if (valorA > valorC) {
    console.log("Esta condicion es verdadera")
}else if (valorC > valorA) {
    console.log("Esta condicion es Falsa")
}else {
    console.log("AMBAS SON FALSAS")
}


// para almacenar un elemento del dom en una variable utilizamos document."selector"
// get

// document.getElementsByClassName = a obtener un elemento del HTML por su clase
// let boton = document.querySelector(".boton-login");
//     boton.addEventListener("onclick", function (){
// })

let contenedor = document.getElementsByClassName("container")[0]
let usuario = document.getElementsByClassName("usuario")[0];
let contraseña = document.getElementsByClassName("contraseña")[0];
// EVENTO ONCLICK DE MANERA IMPLICITA EN HTML. como ejecutar una funcion cuando se le hace click a un boton.
function onClick () {

    contenedor.setAttribute("class", ".hide")

    if (usuario.value == "Martin" && Number(contraseña.value) == "1234") {
        console.log(typeof contraseña.value)
        contenedor.setAttribute("class", "container")
    }
}






function eventOnClick (e) {
    console.log("esta funcion fue disparada por un evento onclick");
}

// Variables 
//  Algo que puede cambiar
// Una forma de almacenar valores en un espacio de memoria.
// Numeros , 1 o 1.1  valor de tipo Number
// Textos, "Mi texto" valor de tipo String


// dom = document object model


